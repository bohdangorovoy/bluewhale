<?php

//Config
$config = array(
	'to'		=> 'help.bluewhale@gmail.com',
	'subject'	=> 'Новое сообщение с сайта "Синий кит"',
	'headers'	=> 'MIME-Version: 1.0' . "\r\n" . 'Content-type: text/html; charset=iso-8859-1' . "\r\n" . 'From: "Синий Кит" <info@xscenter.org>' . "\r\n"
);

//Receive data from POST
$data = array(
	'name' 		=> $_POST['name'] ?: '',
	'phone'		=> $_POST['phone'] ?: '',
	'email'		=> $_POST['email'] ?: '',
	'message' 	=> $_POST['message'] ?: ''
);
//Create message
$message = '
<html>
	<head>
		<title>Новое сообщение с сайта "Синий кит"</title>
	</head>
	<body>
		<h2>' . ucfirst($data['name']) . ' отправил вам сообщение</h2>
		<h3>Контактные данные:</h3>
		<p>Имя: ' . ucfirst($data['name']) . '</p>
		<p>Телефон: ' . $data['phone'] . '</p>
		<p>Email: ' . $data['email'] . '</p>
		<h3>Сообщение:</h3>
		<p>' . $data['message'] . '</p>
	</body>
</html>
';

//Sending mail
if (mail($config['to'], $config['subject'], $message, $config['headers'])) {
	$res = array(
		'type'		=> 'success',
		'message'	=> 'Спасибо, мы получили ваше сообщение!'
	);
} else {
	$res = array(
		'type'		=> 'error',
		'message'	=> 'Не удалось отправить сообщение, попробуйте еще раз.'
	);
}

echo json_encode($res);



